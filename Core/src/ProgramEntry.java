
import FileHandling.FileHandler;
import UI.Placeholder;



/**
 * Created by Admin on 12/20/2015.
 *
 * Main access point to program.
 *
 * TODO: make time counter local
 * TODO: get response from actions that require login -> if such response is received, prompt a new connection
 * TODO: handle no internet connection errors
 *
 */
public class ProgramEntry {


    public static void main(String[] args) throws InterruptedException{


        // create directory if it does not exist
        FileHandler.createHomeDIR();

        // launch chat functionality
        Placeholder placeholder = new Placeholder();
        placeholder.run();
    }
}
