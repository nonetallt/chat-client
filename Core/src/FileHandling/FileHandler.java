package FileHandling;

import Various.Settings;
import Various.Tools;

import java.io.*;

/**
 * Created by Admin on 3/5/2016.
 */
public class FileHandler {

    public static boolean createHomeDIR(){
        File directory = new File(Settings.homeDIR);
        // check if that directory already exists
        if(!directory.exists()){
            return directory.mkdir();
        }
        return true;
    }

    public static boolean preferencesExist(){
        String name = Settings.homeDIR + "\\" + "connectionPreferences.txt";

        File file = new File(name);
        if(file.exists()){
            return true;
        }
        return false;
    }

    public static void writeFile(String name, String content) throws Exception{
        name = Settings.homeDIR + "\\" + name;

        File file = new File(name);

        if(!file.exists()){
            // create it if it doesent exist
            if(!file.createNewFile()) throw new Exception("Preferences file could not be created.");
        }

        FileWriter fileWriter = new FileWriter(name);
        fileWriter.append(content);
        fileWriter.flush();
    }

    public static String readFile(String name) throws Exception{
        name = Settings.homeDIR + "\\" + name;

        File file = new File(name);
        if(file.exists()){
            BufferedReader reader = new BufferedReader(new FileReader(file.getPath()));
            String currentLine;
            String content = "";

            while ( (currentLine = reader.readLine()) != null){
                content += currentLine;
            }
            return content;
        }
        return null;
    }

    public static boolean defaultExists(){
        InputStream test =  FileHandler.class.getResourceAsStream("default.txt");
        if(test == null){
            return false;
        }
        return true;
    }

    public static String loadFromJAR(String name) throws Exception{
        InputStream test =  FileHandler.class.getResourceAsStream(name);
        BufferedReader reader = new BufferedReader(new InputStreamReader(test, "UTF-8"));

        String currentLine;
        String content = "";

        while ( (currentLine = reader.readLine()) != null){
            content += currentLine;
        }
        return content;
    }
}
