package Various;

public class Settings {

	// connection
	public static final String connectionURL = "http://koti.tamk.fi/~c3jmikko/Response.php";
	public static final String registerURL = "http://koti.tamk.fi/~c3jmikko/LoginProxy/Register.html";
	public static final String userAgent = "client v1.0";

	// various
	public static final int messageRequestDelayMS = 500;
	public static final boolean debug = false;
	public static final String homeDIR = "ChatHome";
	public static final String mainFX = "../Resources/main.fxml";
	public static final String loginFX = "../Resources/login.fxml";
	public static final String messageSound = "Core\\src\\Resources\\msg.wav";
	public static final String title  ="ChatClient v1.0";


}
