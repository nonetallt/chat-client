package Various;
import java.util.Base64;

/**
 * Created by Admin on 2/20/2016.
 *
 * parses stuff
 */
public class Parser {

    public static byte[] parseKeyPrivate(String key){
        key = key.replace("-----BEGIN PRIVATE KEY-----","");
        key = key.replace("-----END PRIVATE KEY-----","");
        key = key.replaceAll("\\s","");
        return Base64.getDecoder().decode(key);
    }

    public static byte[] parseKeyPublic(String key){
        key = key.replace("-----BEGIN PUBLIC KEY-----","");
        key = key.replace("-----END PUBLIC KEY-----","");
        key = key.replaceAll("\\s","");
        return Base64.getDecoder().decode(key);
    }

    public static String[] cutKeys(String keys){
        return keys.split("\\|\\|");
    }

    public static String cutID(String line){
        return line.replaceFirst("\\d*\\s*","");
    }

    public static String addStringAfterSender(String message, String string){

        String[] split = message.split("  ",4);
        if(split.length > 1){
            String receive = "(" + split[1] + ")";
            if(!split[2].equals("PUB")){
                receive = "(from: " + split[1] + ")";
            }
            return split[0] + " | " + receive + string + split[3];
        }
        return "";
    }
}
