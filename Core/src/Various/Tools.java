package Various;

import java.security.SecureRandom;
import java.util.ArrayList;

/**
 * Created by Admin on 2/13/2016.
 *
 *
 */
public class Tools {

    public static String genRandomString() throws Exception{
        return Integer.toString(SecureRandom.getInstanceStrong().nextInt());
    }

    public static void printStrings(String[] strings){
        for(int n = 0; n < strings.length; n++){
            // don't print empty
            if(!strings[n].equals("")){
                System.out.println(strings[n] + "\r");
            }
            else{
                //System.out.println("EMPTY STRING");
            }
        }
    }
    public static boolean isContained(ArrayList<Integer> array,int value){
        for(int n = 0; n <  array.size(); n++){
            if(array.get(n) == value){
                return true;
            }
        }
        return false;
    }
    public static void pr(String print){
        System.out.println(print);
    }
    public static void pn(String print) {
        System.out.print(print);
    }
}
