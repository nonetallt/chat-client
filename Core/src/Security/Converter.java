package Security;

import Various.Parser;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * Created by Admin on 2/20/2016.
 *
 * Covert datatypes
 */
public class Converter {

    public static PublicKey bytesToPublicKey(byte[] key){
        // create specification for key type
        X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(key);

        // use key factory class to create key from specification
        PublicKey publicKey = null; // initialize to something

        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            publicKey = keyFactory.generatePublic(pubKeySpec);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return publicKey;
    }

    public static PrivateKey loadPrivateKey(String key64) throws GeneralSecurityException {
        byte[] clear = Parser.parseKeyPrivate(key64);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(clear);
        KeyFactory fact = KeyFactory.getInstance("RSA");
        PrivateKey priv = fact.generatePrivate(keySpec);

        return priv;
    }

    public static PublicKey loadPublicKey(String stored) throws GeneralSecurityException {

        byte [] data = Parser.parseKeyPublic(stored);

        X509EncodedKeySpec spec = new X509EncodedKeySpec(data);
        KeyFactory fact = KeyFactory.getInstance("RSA");
        return fact.generatePublic(spec);
    }

}
