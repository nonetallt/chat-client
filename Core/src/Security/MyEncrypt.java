package Security;
import Various.Tools;

import java.security.Key;
import java.security.PrivateKey;
import java.util.Base64;
import javax.crypto.Cipher;

public class MyEncrypt {

	public static byte[] cryptWithRSA(Key key, String plaintext) throws Exception{
		
		// default to no value
		Cipher cipher = null;
		
		cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.ENCRYPT_MODE,key);
		return cipher.doFinal(plaintext.getBytes());
	}

	public static byte[] cryptWithAES(Key key, String plaintext,TempData temp) throws Exception{

		// default to no value
		Cipher cipher = null;

		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE,key);

		temp.setLastIV(cipher.getIV());

		return cipher.doFinal(plaintext.getBytes());
	}

	public static byte[] cryptWithAESbinary(Key key, byte[] bytes,TempData temp) throws Exception{

		// default to no value
		Cipher cipher = null;

		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE,key);

		temp.setLastIV(cipher.getIV());

		return cipher.doFinal(bytes);
	}
}
