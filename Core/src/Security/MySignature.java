package Security;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;

public class MySignature {
	
	public static Signature getSignatureRSA() throws Exception{
		Signature sign = null;
		sign = Signature.getInstance("SHA256withRSA");
		return sign;
	}
	
	public static byte[] signData(byte[] data, PrivateKey key) throws Exception{
		
		Signature signature = getSignatureRSA();
		signature.initSign(key);
		signature.update(data);
		return signature.sign();
	}
	
	public static boolean verify(PublicKey key, byte[] signedData, byte[] cipherText) throws Exception{

		Signature sign = getSignatureRSA();

		// initialize to return value to false
		boolean verifies = false;

		// initialize signature for verifying
		sign.initVerify(key);
		
		// input data
		sign.update(cipherText);
		
		// see if signature verifies
		verifies = sign.verify(signedData);
		
		return verifies;
	}

}
