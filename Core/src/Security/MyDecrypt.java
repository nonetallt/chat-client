package Security;

import java.security.Key;
import java.security.PublicKey;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;

public class MyDecrypt {
	
	public static String decryptWithRSA(Key key, byte[] cipherText) throws Exception{
		
		Cipher cipher = null;
		cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.DECRYPT_MODE,key);
		return new String(cipher.doFinal(cipherText));
	}

	public static String decryptWithAES(Key key, byte[] cipherText, byte[] iv) throws Exception{

		Cipher cipher = null;
		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE,key,new IvParameterSpec(iv));
		return new String(cipher.doFinal(cipherText));
	}

}
