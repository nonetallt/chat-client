package Security;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class KeyGen {
	
	// const
	public KeyGen(){
		
	}
	
	public static KeyPair genKeys() throws NoSuchAlgorithmException{
		KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
		generator.initialize(2048,SecureRandom.getInstanceStrong());
		return generator.generateKeyPair();
	}

	public static SecretKey genAES() throws NoSuchAlgorithmException{
		KeyGenerator keyGen = KeyGenerator.getInstance("AES");
		keyGen.init(128,SecureRandom.getInstanceStrong());
		return keyGen.generateKey();
	}

}
