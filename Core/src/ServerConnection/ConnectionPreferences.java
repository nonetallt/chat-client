package ServerConnection;

import FileHandling.FileHandler;
import Security.Converter;
import UI.Controller;
import Various.Parser;
import Various.Tools;

import javax.crypto.SecretKey;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;
import java.util.prefs.Preferences;

/**
 * Created by Admin on 2/24/2016.
 */
public class ConnectionPreferences {

    private String id;
    private PrivateKey privateKey;
    private PublicKey publicKey;
    private SecretKey serverSymmetricKey;
    private Controller controller;
    private PublicKey serverPublicKey;
    private String account;

    // constructor
    public ConnectionPreferences() throws Exception{
        init();
    }

    // initializes values for usage
    private void init() throws Exception{

        // load id and keys from preferences if they exist
        if(FileHandler.preferencesExist()){
            String[] data = Parser.cutKeys(FileHandler.readFile("connectionPreferences.txt"));
            id = data[0];
            publicKey = Converter.loadPublicKey(data[1]);
            privateKey = Converter.loadPrivateKey(data[2]);
        }
        // else load from file contained within jar
        else if(defaultExists()){
            String[] data = Parser.cutKeys(FileHandler.loadFromJAR("default.txt"));
            id = data[0];
            publicKey = Converter.loadPublicKey(data[1]);
            privateKey = Converter.loadPrivateKey(data[2]);
        }
        else{
            throw new Exception("Could not initialize connection keys or client id.");
        }
    }

    public String getMyID(){
        return id;
    }

    public PublicKey getMyPublicKey(){
        return publicKey;
    }

    public PrivateKey getMyPrivateKey(){
        return privateKey;
    }

    private boolean defaultExists(){
        if(FileHandler.defaultExists()) return true;
        return false;
    }

    public void setPreferencesData(String id, KeyPair keys) throws Exception{
        String content = "";
        content += id;
        content += "||";
        content += Base64.getEncoder().encodeToString(keys.getPublic().getEncoded());
        content += "||";
        content += Base64.getEncoder().encodeToString(keys.getPrivate().getEncoded());
        FileHandler.writeFile("connectionPreferences.txt",content);
    }

    public SecretKey getServerSymmetricKey() {
        return serverSymmetricKey;
    }

    public void setServerSymmetricKey(SecretKey serverSymmetricKey) {
        this.serverSymmetricKey = serverSymmetricKey;
    }

    public Controller getController() {
        if(controller == null ){
            Tools.pr("Warning: controller value is null in preferences.");
        }
        return controller;
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public void setServerPublicKey(PublicKey serverPublicKey) {
        this.serverPublicKey = serverPublicKey;
    }

    public PublicKey getServerPublicKey() {
        return serverPublicKey;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
