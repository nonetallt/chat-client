package ServerConnection;
import Security.*;
import UI.Controller;
import Various.Settings;
import Various.Tools;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import javax.crypto.SecretKey;
import java.net.URLEncoder;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;

/**
 * Created by Admin on 12/20/2015.
 * Used to send POST requests to server defined in settings
 *
 * Header types:
 * - message 	, send messages
 * - request 	, get messages
 * - login   	, initiate new connection and associate account with connection
 * - time		, get server time
 *
 */

public class Connector {

	private static String[] send(String header, String content){

		try{
			// encode url content that's about to be sent
			header = URLEncoder.encode(header,"UTF-8");
			content = URLEncoder.encode(content,"UTF-8");

			// send data
			Document response = Jsoup.connect(Settings.connectionURL)
			.userAgent(Settings.userAgent)
			.data("account", header)
			.data("password", content)
			.post();

			// <p> elements from response page
			Elements paragraphs = response.getElementsByTag("p");

			//------------ FOR DEBUGGING ------------
			if(Settings.debug){
				Elements debug = response.getElementsByTag("h1");
				for(int n = 0; n < debug.size(); n++){
					Tools.pr("--->Server debug: " + debug.get(n).text());
				}
			}
			//------------ FOR DEBUGGING ------------

			if(paragraphs.size() == 0){
				return new String[]{"-Empty response from server-"};
			}
			else{
				// create return object
				String[] toBeReturned = new String[paragraphs.size()];

				for(int n = 0; n < paragraphs.size(); n++){
					toBeReturned[n] = paragraphs.get(n).text();
				}
				return toBeReturned;
			}
		}
		catch(Exception e){
			//e.printStackTrace();
			return new String[]{"-Could not connect to server.-"};
		}
	}
	private static String[] sendMSG(String header, String content, String message){

		try{
			// encode url content that's about to be sent
			header = URLEncoder.encode(header,"UTF-8");
			content = URLEncoder.encode(content,"UTF-8");
			message = URLEncoder.encode(message,"UTF-8");

			// send data
			Document response = Jsoup.connect(Settings.connectionURL)
					.userAgent(Settings.userAgent)
					.data("header", header)
					.data("content", content)
					.data("message", message)
					.post();

			// <p> elements from response page
			Elements paragraphs = response.getElementsByTag("p");

			//------------ FOR DEBUGGING ------------
			if(Settings.debug){
				Elements debug = response.getElementsByTag("h1");
				for(int n = 0; n < debug.size(); n++){
					Tools.pr("--->Server debug: " + debug.get(n).text());
				}
			}
			//------------ FOR DEBUGGING ------------

			if(paragraphs.size() == 0){
				return new String[]{"-Empty response from server-"};
			}
			else{
				// create return object
				String[] toBeReturned = new String[paragraphs.size()];

				for(int n = 0; n < paragraphs.size(); n++){
					toBeReturned[n] = paragraphs.get(n).text();
				}
				return toBeReturned;
			}
		}
		catch(Exception e){
			//e.printStackTrace();
			return new String[]{"-Could not connect to server.-"};
		}
	}

	public static void establishSecureConnection(ConnectionPreferences preferences,String account,String password) throws Exception{

		Controller gui = preferences.getController();

		gui.addStringToMessageArea("Establishing secure connection..");

		gui.addStringToMessageArea("Getting connection preferences..");
		PrivateKey privkey = preferences.getMyPrivateKey();
		String id = preferences.getMyID();

		gui.addStringToMessageArea("Requesting connection from server..");
		PublicKey serverKey = requestConnection(id,privkey);

		gui.addStringToMessageArea("Verifying received public key..");
		if(!verifyServerIdentity(id,privkey,serverKey)) throw new Exception("Server identity could not be confirmed.");
		preferences.setServerPublicKey(serverKey);

		gui.addStringToMessageArea("Sending a new symmetric key to server..");
		SecretKey key = sendSymmetricKey(id, privkey, serverKey);
		preferences.setServerSymmetricKey(key);

		// prepare new keys for next connection
		gui.addStringToMessageArea("Preparing keys for next login..");
		sendNextConnectionKey(id,privkey,preferences.getServerSymmetricKey(),preferences);

		// send login info to server
		gui.addStringToMessageArea("Logging in as " + account + "..");
		login(id,privkey,preferences.getServerSymmetricKey(),account,password);
	}
	private static PublicKey requestConnection(String id,PrivateKey privateKey) throws Exception{

		// message string
		String message = "client:" + id;

		// sign message with loaded private key
		byte[] signedMessage = MySignature.signData(message.getBytes(), privateKey);

		// convert signature  to string
		String signature = "|signature:" + Base64.getEncoder().encodeToString(signedMessage);

		String[] response = Connector.send("connectionRequest",message + signature);

		//Tools.printStrings(response);

		return Converter.loadPublicKey(response[0]);
	}
	private static boolean verifyServerIdentity(String id, PrivateKey myKey,PublicKey unverifiedServerKey) throws Exception{

		// crypt and sign some random data
		String randomData1 = Tools.genRandomString();
		String randomData2 = Tools.genRandomString();
		byte[] step1 = MyEncrypt.cryptWithRSA(myKey,randomData1); // crypt with client private key
		byte[] step2 = MyEncrypt.cryptWithRSA(unverifiedServerKey,randomData2); // crypt with "server" public key

		// form message string
		String message = "";
		message += "client:" + id;
		message += "|";
		message += "data1:" + Base64.getEncoder().encodeToString(step1);
		message += "|";
		message += "data2:" + Base64.getEncoder().encodeToString(step2);

		// sign message
		byte[] signedMessage = MySignature.signData(message.getBytes(), myKey);

		// convert signature  to string
		String signature = "|signature:" + Base64.getEncoder().encodeToString(signedMessage);

		String[] response = Connector.send("identityVerificationRequest",message + signature);
		boolean verify = MySignature.verify(unverifiedServerKey,Base64.getDecoder().decode(response[2]),(response[0] + response[1]).getBytes());

		if(!verify){
			Tools.pr("Provided public key does not match the signature from server!");
			return false;
		}
		if(!response[0].equals(randomData1) || !response[1].equals(randomData2)){
			Tools.pr("Public key provider is not trusted!");
			return false;
		}

		return true;
	}
	private static SecretKey sendSymmetricKey(String id, PrivateKey myKey, PublicKey serverKey) throws Exception{

		// create new AES key
		SecretKey connectionKey = KeyGen.genAES();
		String symmetricKey = Base64.getEncoder().encodeToString(connectionKey.getEncoded());
		//encrypt it with servers public key
		byte[] keyCrypt = MyEncrypt.cryptWithRSA(serverKey,symmetricKey);

		// message string
		String message = "";
		message += "client:" + id;
		message += "|";
		message += "symmetricKey:" + Base64.getEncoder().encodeToString(keyCrypt);

		// sign message with loaded private key
		byte[] signedMessage = MySignature.signData(message.getBytes(), myKey);

		// convert signature  to string
		String signature = "|signature:" + Base64.getEncoder().encodeToString(signedMessage);

		Connector.send("saveSymmetricKey",message + signature);
		return connectionKey;
	}
	private static void login(String id,PrivateKey myKey, SecretKey symmetricKey, String account,String password) throws Exception{

		// crypt account and password
		TempData temp = new TempData();
		String accCrypted = Base64.getEncoder().encodeToString(MyEncrypt.cryptWithAES(symmetricKey,account,temp));
		byte[] iv1 = temp.getLastIV();
		String pwCrypted = Base64.getEncoder().encodeToString(MyEncrypt.cryptWithAES(symmetricKey,password,temp));
		byte[] iv2 = temp.getLastIV();

		// message string
		String message = "";
		message += "client:" + id;
		message += "|";
		message += "account:" + accCrypted;
		message += "|";
		message += "password:" + pwCrypted;
		message += "|";
		message += "accountIV:" + Base64.getEncoder().encodeToString(iv1);
		message += "|";
		message += "passwordIV:" + Base64.getEncoder().encodeToString(iv2);

		// sign message with loaded private key
		byte[] signedMessage = MySignature.signData(message.getBytes(), myKey);

		// convert signature  to string
		String signature = "|signature:" + Base64.getEncoder().encodeToString(signedMessage);

		String[] response = Connector.send("login",message + signature);
		if(response[0].contains("could not log in")){
			throw new Exception("Login information is invalid.");
		}
		else if(response[0].contains("connection expired")){
			throw new Exception("This connection has expired.");
		}
	}
	public static void sendMessage(String id, PrivateKey privateKey, String userMessage, String receiver, SecretKey symmetricKey) throws Exception{

		TempData temp = new TempData();
		byte[] msgCrypt = MyEncrypt.cryptWithAES(symmetricKey,userMessage,temp);
		userMessage = Base64.getEncoder().encodeToString(msgCrypt);
		String iv = Base64.getEncoder().encodeToString(temp.getLastIV());
		String msgSign = Base64.getEncoder().encodeToString(MySignature.signData(userMessage.getBytes(), privateKey));

		// message string
		String message = "";
		message += "client:" + id;
		message += "|";
		message += "messageSign:" + msgSign;
		message += "|";
		message += "receiver:" + receiver;
		message += "|";
		message += "messageIV:" + iv;

		// sign message with loaded private key
		byte[] signedMessage = MySignature.signData(message.getBytes(), privateKey);

		// convert signature  to string
		String signature = "|signature:" + Base64.getEncoder().encodeToString(signedMessage);

		Connector.sendMSG("message", message + signature, userMessage);
	}
	public static String[] getMessages(String id, PrivateKey privateKey) throws Exception{
		// message string
		String message = "";
		message += "client:" + id;

		// sign message with loaded private key
		byte[] signedMessage = MySignature.signData(message.getBytes(), privateKey);

		// convert signature  to string
		String signature = "|signature:" + Base64.getEncoder().encodeToString(signedMessage);

		String[] response = Connector.send("request",message + signature);
		return response;
	}
	public static void sendNextConnectionKey(String id,PrivateKey myKey, SecretKey symmetricKey, ConnectionPreferences prefs) throws Exception{

		// create new keys
		KeyPair newKeys = KeyGen.genKeys();
		prefs.setPreferencesData(id,newKeys);
		String key = Base64.getEncoder().encodeToString(newKeys.getPublic().getEncoded());

		// crypt new key
		TempData temp = new TempData();
		String keyEncrypted = Base64.getEncoder().encodeToString(MyEncrypt.cryptWithAES(symmetricKey, key, temp));
		byte[] iv1 = temp.getLastIV();

		// message string
		String message = "";
		message += "client:" + id;
		message += "|";
		message += "publicKey:" + keyEncrypted;
		message += "|";
		message += "keyIV:" + Base64.getEncoder().encodeToString(iv1);

		// sign message with loaded private key
		byte[] signedMessage = MySignature.signData(message.getBytes(), myKey);

		// convert signature  to string
		String signature = "|signature:" + Base64.getEncoder().encodeToString(signedMessage);

		String[] response = Connector.send("saveNextPublicKey",message + signature);
		//Tools.printStrings(response);
	}

}
