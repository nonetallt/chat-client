package ServerConnection;

import Security.MyDecrypt;
import Security.MySignature;
import UI.Placeholder;
import Various.Parser;
import Various.Settings;
import Various.Tools;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;

/**
 * Created by Admin on 2/15/2016.
 *
 * Store information regarding connections
 */
public class History {

    private String programStarted;
    private ArrayList<Integer> printedMessageIDs;
    private ConnectionPreferences connectionPreferences;
    private MediaPlayer mediaPlayer;
    private Media sound;
    private Placeholder ui;

    // const
    public History(ConnectionPreferences connectionPreferences, Placeholder ui){
        programStarted = createTimestamp();
        printedMessageIDs = new ArrayList<>();
        this.connectionPreferences = connectionPreferences;
        this.ui = ui;

        // PLACEHOLDER sounds

        try {
            sound = new Media(History.class.getResource("msg.wav").toURI().toString());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public static String createTimestamp(){
        Date date = new Date();
        Timestamp myTimestamp = new Timestamp(date.getTime());
        return new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(myTimestamp);
    }
    public void printNewMessages(String[] response){

        if(connectionPreferences.getController() == null) return;

        // decrypt messages gathered from server
        String[] decryptRequest = decryptRequest(response);

        if(decryptRequest == null ){
            return;
        }

        // create array object to contain message id values
        int[] messageIDs = new int[decryptRequest.length];

        // get id from each response string
        for(int n = 0; n < decryptRequest.length; n++){
            messageIDs[n] = getID(decryptRequest[n]);
        }

        boolean once = true;

        // print messages
        for(int n = 0; n < decryptRequest.length; n++){
            // only print if message with same id hasn't been printed before
            if(!Tools.isContained(printedMessageIDs,messageIDs[n])){
                String text = Parser.cutID(decryptRequest[n]);
                connectionPreferences.getController().addStringToMessageArea(Parser.addStringAfterSender(text,": "));
                // add message id to already printed list
                printedMessageIDs.add(messageIDs[n]);
                if(once && notMe(text)){
                    if(sound != null){
                        mediaPlayer = new MediaPlayer(sound);
                        mediaPlayer.setVolume(0.2);
                        mediaPlayer.play();
                    }
                }
               once = false;
            }
        }

    }

    // private parsing
    private String[] decryptRequest(String[] response){

        String[] returned = new String[response.length];

        for(int n = 0; n < returned.length; n++){
            String[] split = Parser.cutKeys(response[n]);
            if(split.length > 1){
                try {
                    byte[] cipherText = Base64.getDecoder().decode(split[0]);
                    byte[] iv = Base64.getDecoder().decode(split[1]);
                    byte[] signature = Base64.getDecoder().decode(split[2]);

                    byte[]content = (split[0] + "||" + split[1] + "||").getBytes();

                    if(MySignature.verify(connectionPreferences.getServerPublicKey(),signature,content)){
                        returned[n] = MyDecrypt.decryptWithAES(connectionPreferences.getServerSymmetricKey(),cipherText,iv);
                    }
                    else{
                        returned[n] = "";
                        Tools.pr("Warning: server message sign does not match.");
                    }
                } catch (Exception e) {
                    return null;
                }
            }
            else{
                returned[n] = "";
            }
        }
        return returned;
    }
    private int getID(String responseString){

        String temp = "-1";

        for(int n = 0; n < responseString.length(); n++){
            if(responseString.substring(n,n+1).equals(" ")){
                return Integer.parseInt(temp);
            }
            else{
                temp += responseString.substring(n,n+1);
            }
        }
        return Integer.parseInt(temp);
    }
    private boolean notMe(String message){
        String[] split = message.split("  ",3);
        if(split.length < 3) return false;
        String user = split[1];
        if(ui.getAccount().equals(user)){
            return false;
        }
        return true;
    }

}
