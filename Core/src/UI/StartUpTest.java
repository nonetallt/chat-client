package UI;

import ServerConnection.ConnectionPreferences;
import Various.Settings;
import com.sun.deploy.uitoolkit.impl.fx.HostServicesFactory;
import com.sun.javafx.application.HostServicesDelegate;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.util.concurrent.CountDownLatch;

public class StartUpTest extends Application {


    public static final CountDownLatch latch = new CountDownLatch(1);
    public static StartUpTest startUpTest = null;
    private Controller controller;
    private ControllerLogin controllerLogin;
    private FXMLLoader loader;
    private Stage stage;
    private HostServicesDelegate hostServices;
    private Placeholder ui;

    public static StartUpTest waitForStartUpTest() {
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return startUpTest;
    }

    public static void setStartUpTest(StartUpTest startUpTest0) {
        startUpTest = startUpTest0;
        latch.countDown();
    }

    // const
    public StartUpTest() {
        setStartUpTest(this);
    }


    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        loader = new FXMLLoader(getClass().getResource("login.fxml"));
        Parent root = loader.load();
        controllerLogin = loader.getController();
        stage.setScene(new Scene(root));
        stage.setTitle(Settings.title);
        stage.show();
        hostServices = HostServicesFactory.getInstance(this);

    }

    public void changeToMainScreen() {
        try {
            replaceSceneContent("main.fxml");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void replaceSceneContent(String fxml) throws Exception {
        loader = new FXMLLoader(getClass().getResource(fxml));
        Parent root = loader.load();

        if(loader.getController() instanceof Controller){
            controller = loader.getController();
        }
        if(loader.getController() instanceof ControllerLogin){
            controllerLogin = loader.getController();
        }

        stage.getScene().setRoot(root);
    }

    public Controller getController(){
        return controller;
    }

    public ControllerLogin getControllerLogin(){
        return controllerLogin;
    }

    public void initWithPreferences(ConnectionPreferences connectionPreferences) {
        controller.myInit(connectionPreferences);
    }

    public void initWithPH(Placeholder test) {
        controllerLogin.myInit(test);
    }

    public HostServicesDelegate getWeb() {
        return hostServices;
    }

    public void initializeClose(Placeholder ui){
        this.ui = ui;

        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                System.out.println("Stage is closing");
                ui.programExit(100);
            }
        });
    }
}