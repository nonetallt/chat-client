package UI;

import Security.MyDecrypt;
import ServerConnection.ConnectionPreferences;
import ServerConnection.Connector;
import ServerConnection.History;
import Various.Settings;
import Various.Tools;

import java.security.PrivateKey;

/**
 * Created by Admin on 2/15/2016.
 *
 * Send repeated requests to get new messages that have been sent to server
 */
public class MessageGetter extends Thread{

    private History history;
    private boolean connected;
    private String id;
    private  PrivateKey key;

    // constructor
    public MessageGetter(String id, PrivateKey key, ConnectionPreferences prefs,Placeholder ui){
        this.id = id;
        this.key = key;
        connected = false;
        history = new History(prefs,ui);

        // get all messages first
        //String[] response = Connector.send("request", "");

        // update history
        //history.updateLastRequest();
        //Tools.printStrings(response);
    }

    @Override public void run(){

        while(connected){
            String[] response = new String[0];
            try {
                response = Connector.getMessages(id, key);
            } catch (Exception e) {
            }
            history.printNewMessages(response);
            try{
                Thread.sleep(Settings.messageRequestDelayMS);
            }catch(InterruptedException e){
                Tools.pr("Error in message getter thread.");
            }
        }
    }


    public void setConnected(boolean connected){
        this.connected = connected;
    }

    public synchronized History getHistory(){
        return history;
    }
}
