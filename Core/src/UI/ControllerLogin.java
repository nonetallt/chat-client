package UI;

import Various.Settings;
import Various.Tools;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Admin on 3/10/2016.
 */
public class ControllerLogin implements Initializable{

    private Placeholder ui;

    @FXML
    private TextField accountField;

    @FXML
    private PasswordField passwordField;

    @FXML
    protected void tryLogin(ActionEvent event) {
        ui.setAccount(accountField.getText());
        ui.setPassword(passwordField.getText());
        ui.setWaiting(false);
    }

    @FXML
    protected void openLinkInBrowser(ActionEvent event) {
        try{
            ui.openLink(Settings.registerURL);
        }catch (Exception e){}
    }

    @FXML
    protected void changeSelectionToPW(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            passwordField.requestFocus();
        }
    }

    @FXML
    protected void enterLogin(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            ui.setAccount(accountField.getText());
            ui.setPassword(passwordField.getText());
            ui.setWaiting(false);
        }
    }


    @Override public void initialize(URL url, ResourceBundle bundle){

    }

    public void myInit(Placeholder ui){
        this.ui = ui;
    }
}
