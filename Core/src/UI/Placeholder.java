package UI;

import ServerConnection.ConnectionPreferences;
import ServerConnection.Connector;

/**
 * Created by Admin on 2/15/2016.
 *
 */
public class Placeholder {

    private ConnectionPreferences preferences;
    private MessageGetter messageGetter;
    private boolean waiting;
    private StartUpTest startUpTest;
    private String account;
    private String password;

    public Placeholder(){
        waiting = true;
        account = "";
        password = "";

        try {
            preferences = new ConnectionPreferences();
        } catch (Exception e) {
            System.out.println("Could not create connection preferences: " + e.getMessage());
            programExit(5000);
        }
    }

    public void run(){

        // launch fx environment
        new Thread() {
            @Override
            public void run() {
                javafx.application.Application.launch(StartUpTest.class);
            }
        }.start();
        startUpTest = StartUpTest.waitForStartUpTest();

        // --- end

        while(startUpTest.getControllerLogin() == null){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        startUpTest.initializeClose(this);
        startUpTest.initWithPH(this);

        while(waiting){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        startUpTest.changeToMainScreen();
        startUpTest.initWithPreferences(preferences);
        preferences.setController(startUpTest.getController());

        try {
            Connector.establishSecureConnection(preferences,account,password);
            startUpTest.getController().addStringToMessageArea("\nLogged in successfully.");
        } catch (Exception e) {
            System.out.println("Could not create a secure connection: " + e.getMessage());
            startUpTest.getController().addStringToMessageArea("\nCould not log in with the information provided.");
            programExit(5000);
        }

        messageGetter = new MessageGetter(preferences.getMyID(),preferences.getMyPrivateKey(), preferences, this);
        messageGetter.setConnected(true);
        messageGetter.run();
    }

    public void programExit(int ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
        }
        System.exit(0);
    }

    public synchronized void setWaiting(boolean waiting){
        this.waiting = waiting;
    }

    public synchronized void changeToMainScreen(){
        startUpTest.changeToMainScreen();
    }

    public synchronized void openLink(String link){
        startUpTest.getWeb().showDocument(link);
    }

    public synchronized void setAccount(String account) {
        this.account = account;
        preferences.setAccount(account);
    }

    public synchronized void setPassword(String password) {
        this.password = password;
    }

    public String getAccount(){
        return account;
    }
}
