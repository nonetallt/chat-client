package UI;

import ServerConnection.ConnectionPreferences;
import ServerConnection.Connector;
import ServerConnection.History;
import Various.Tools;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;


public class Controller {

    private ConnectionPreferences connectionPreferences;

    @FXML private TextArea test;
    @FXML private TextArea messageArea;
    @FXML private TextField receiverField;


    @FXML
    protected void handleSubmitButtonAction(ActionEvent event) {
        sendMessage();
    }

    @FXML
    public void handleEnterPressed(KeyEvent event){
        if (event.getCode() == KeyCode.ENTER) {
            test.setText(test.getText().replaceAll("\n$",""));
            sendMessage();
        }
    }

    public void myInit(ConnectionPreferences prefs){
        connectionPreferences = prefs;
    }

    private void sendMessage(){
        String message = test.getText();

        if(message.equals("") || message == null){
            return;
        }
        else if(!receiverField.getText().equals(connectionPreferences.getAccount()) && !receiverField.getText().equals("PUB")){
            // message was private so store it locally for display
            addStringToMessageArea(History.createTimestamp() + " | " + "(to: " + receiverField.getText() +"): " + message);
        }

        // if text value is not empty
        try {
            Connector.sendMessage(connectionPreferences.getMyID(), connectionPreferences.getMyPrivateKey(), message, receiverField.getText(),connectionPreferences.getServerSymmetricKey());
        } catch (Exception e) {
            addStringToMessageArea("Message could not be sent: " + e.getMessage());
        }
        // empty text field
        test.setText("");
    }

    public void addStringsToMessageArea(String[] strings){

        for(String line : strings){
            messageArea.appendText(line + "\n");
        }
    }

    public void addStringToMessageArea(String string){
        messageArea.appendText(string + "\n");
    }

}
